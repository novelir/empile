# Empile - A tool to build piles

[**Novelir is still in heavy development.**](https://novelir.org/)


## Dependencies

### Build dependencies

 - qt6-base
 - cmake
 - make

## Installation

    $ mkdir build
    $ cd build
    $ cmake -DCMAKE_INSTALL_PREFIX:PATH=<PREFIX> ..
    $ make
    # make install

Default `<PREFIX>` is `/usr/local`


## Usage

    $ empile path/to/manifest.json -o dest


## Links

- **Get involved at [Novelir.org](https://novelir.org/)**
- **Lean more on the [Wiki](https://gitlab.com/novelir/website/-/wikis)**
