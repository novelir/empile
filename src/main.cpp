/****************************************************************************
**
** Copyright © 2021 Arthur Autin, Novelir
**
** This file is part of Novelir.
**
** Novelir is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Novelir is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Novelir. If not, see <https://www.gnu.org/licenses/>.
**
****************************************************************************/

#include <QCoreApplication>
#include <QCommandLineParser>
#include <QProcess>
#include <QSettings>
#include <QLoggingCategory>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QStandardPaths>
#include <QDir>
#include <QUrl>
#include <QRandomGenerator>

#include "main.hpp"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QCoreApplication::setOrganizationName("novelir");
    QCoreApplication::setApplicationName("empile");
    QCoreApplication::setApplicationVersion("0.0.2");


// Command parser declaration
    QCommandLineParser parser;

    parser.setApplicationDescription("A tool to build piles");

    parser.addHelpOption();

    QCommandLineOption name({"n", "name"}, "Name of the pile", "name");
    QCommandLineOption dependency({"d", "dependency"}, "Install unresolved dependencies");
    QCommandLineOption ignore({"i", "ignore"}, "Do not check unresolved dependencies");
    QCommandLineOption output({"o", "output"}, "Path where the files should be created", "path");
    parser.addOptions({name, dependency, ignore, output});

    parser.addPositionalArgument("manifest", "Path to the manifest.json|yaml file", "manifest.json|yaml");

    parser.process(QCoreApplication::arguments());


// Check command syntax
    if ( parser.positionalArguments().size() != 1 ) {
        QMessageLogger().critical("Empile needs one argument");
        return 1;
    }

    if ( !parser.isSet(name) ) {
        QMessageLogger().critical("No pile name specified");
        return 1;
    }

    QString outputDir = QDir::currentPath() == '/' ? QUrl(QDir::currentPath()).resolved(QUrl(parser.value(output))).toString()
                                                   : QUrl(QDir::currentPath().append('/')).resolved(QUrl(parser.value(output))).toString();
    outputDir.append("/" + parser.value(name).toUtf8());

    if ( !parser.isSet(output) ) {
        QMessageLogger().critical("No output specified");
        return 1;
    }
    else {
        QDir().mkpath(outputDir);
        if ( !QDir(outputDir).isEmpty() ) {
            QMessageLogger().critical("%s is not empty", qUtf8Printable(outputDir));
            return 1;
        }
        if ( outputDir.right(1) == '/' ) {
            outputDir.chop(1);
        }
    }


// Declare the json file
    QString manifestYaml = "";
    QString manifest = QDir::currentPath() == '/' ? QUrl(QDir::currentPath()).resolved(QUrl(parser.positionalArguments()[0])).toString()
                                                  : QUrl(QDir::currentPath().append('/')).resolved(QUrl(parser.positionalArguments()[0])).toString();
    if ( !manifest.endsWith("manifest.json") && !manifest.endsWith("manifest.yaml") ) {
        QMessageLogger().critical("\"%s\" is not a \"manifest.json\" or \"manifest.yaml\" file", qUtf8Printable(manifest));
        return 1;
    }

// Convert yaml into json if needed
    else if ( manifest.endsWith("manifest.yaml") ) {
        manifestYaml = manifest;
        manifest.chop(QString("manifest.yaml").size());
        manifest = manifest + "manifest.json";

        QProcess *yamlToJson = new QProcess();
        yamlToJson->setStandardOutputFile(manifest);
        yamlToJson->start("fy-dump", QStringList() << "--mode" << "json" << manifestYaml );
        yamlToJson->waitForFinished(-1);

        if ( yamlToJson->exitCode() != 0 ) {
            QMessageLogger().critical("\"%s\" could not be converted in json", qUtf8Printable(manifestYaml));
            return 1;
        }
    }


// Open manifest.json
    QJsonDocument jsonDocument;
    {
        QFile file(manifest);
        if ( file.exists() && file.open(QIODevice::ReadOnly | QIODevice::Text) ) {
            jsonDocument = QJsonDocument::fromJson(file.readAll());
            file.close();
        }
        else {
            QMessageLogger().critical("\"%s\" is invalid", qUtf8Printable(manifest));
            return 1;
        }
    }


// Check dependencies
    if ( !parser.isSet(ignore) ) {
        QSettings pileDb(QSettings::SystemScope, a.organizationName(), "pile");
        QStringList unresolved({});
        for ( QJsonValue dep : jsonDocument.object()["dependency-build"].toArray() ) {
            if ( !pileDb.contains(dep.toString()) ) {
                unresolved << dep.toString();
            }
        }
        unresolved.removeDuplicates();
        if ( !unresolved.isEmpty() ) {
            if ( parser.isSet(dependency) ) {
                QProcess *depInstall = new QProcess();
                depInstall->setProcessChannelMode(QProcess::ForwardedChannels);
                depInstall->setInputChannelMode(QProcess::ForwardedInputChannel);
                depInstall->start("pile", QStringList() << "-i" << unresolved.join(',') );
                depInstall->waitForFinished(-1);

                if ( depInstall->exitCode() != 0 ) {
                    QMessageLogger().critical("Cannot install dependencies :\n - %s", qUtf8Printable(unresolved.join(",\n - ")));
                    return 1;
                }
                else {
                    QMessageLogger().info("Correctly installed dependencies :\n - %s", qUtf8Printable(unresolved.join(",\n - ")));
                }

            }
            else {
                QMessageLogger().critical("Unresolved dependencies :\n - %s", qUtf8Printable(unresolved.join(",\n - ")));
                return 1;
            }
        }
    }


// Follow the instructions for each job
    QString empileDir;
    do {
        empileDir = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation)[0] + '/' + QString::number(QRandomGenerator::global()->generate64());
    } while ( QDir(empileDir).exists() );

    for ( QJsonValue value : jsonDocument.object()["build"].toArray() ) {
        QString id = value["id"].toString();
        QMessageLogger().info("\n\n\"%s\" job beginning", qUtf8Printable(id));

    // Create the job's directory
        QString jobDir = empileDir + '/' + id;
        QDir(jobDir).removeRecursively();
        QDir().mkpath(jobDir);

    // Create the Novelir Filesystem Hierarchy
        QDir().mkpath(jobDir + "/output/resource");
        QDir().mkdir(jobDir + "/output/resource/immutable");
        QDir().mkdir(jobDir + "/output/resource/immutable/data");
        QDir().mkdir(jobDir + "/output/resource/immutable/fonts");
        QDir().mkdir(jobDir + "/output/resource/immutable/headers");
        QDir().mkdir(jobDir + "/output/resource/immutable/icons");
        QDir().mkdir(jobDir + "/output/resource/immutable/libraries");
        QDir().mkdir(jobDir + "/output/resource/immutable/mime");
        QDir().mkdir(jobDir + "/output/resource/immutable/themes");
        QDir().mkdir(jobDir + "/output/resource/mutable");
        QDir().mkdir(jobDir + "/output/resource/mutable/cache");
        QDir().mkdir(jobDir + "/output/resource/mutable/configuration");
        QDir().mkdir(jobDir + "/output/resource/mutable/data");
        QDir().mkdir(jobDir + "/output/resource/program");
        QDir().mkdir(jobDir + "/output/runtime");
        QDir().mkdir(jobDir + "/output/runtime/host");
        QDir().mkdir(jobDir + "/output/runtime/host/media");
        QDir().mkdir(jobDir + "/output/runtime/host/temporary");
        QDir().mkdir(jobDir + "/output/runtime/kernel");
        QDir().mkdir(jobDir + "/output/runtime/kernel/boot");
        QDir().mkdir(jobDir + "/output/runtime/kernel/dev");
        QDir().mkdir(jobDir + "/output/runtime/kernel/proc");
        QDir().mkdir(jobDir + "/output/runtime/kernel/sys");
        QDir().mkdir(jobDir + "/output/runtime/user");

    // Create the Filesystem Hierarchy Standard Layer
        QFile().link("resource/program", jobDir + "/output/bin");
        QFile().link("runtime/kernel/boot", jobDir + "/output/boot");
        QFile().link("runtime/kernel/dev", jobDir + "/output/dev");
        QFile().link("resource/mutable/configuration", jobDir + "/output/etc");
        QFile().link("resource/immutable/headers", jobDir + "/output/include");
        QFile().link("resource/immutable/libraries", jobDir + "/output/lib");
        QFile().link("resource/immutable/libraries", jobDir + "/output/lib64");
        QFile().link("resource/immutable/libraries", jobDir + "/output/libexec");
        QFile().link(".", jobDir + "/output/local");
        QFile().link("runtime/host/media", jobDir + "/output/media");
        QFile().link("runtime/kernel/proc", jobDir + "/output/proc");
        QFile().link("runtime/host", jobDir + "/output/run");
        QFile().link("resource/program", jobDir + "/output/sbin");
        QFile().link("resource/immutable/data", jobDir + "/output/share");
        QFile().link("resource/mutable/data", jobDir + "/output/srv");
        QFile().link("runtime/kernel/sys", jobDir + "/output/sys");
        QFile().link("runtime/host/temporary", jobDir + "/output/tmp");
        QFile().link(".", jobDir + "/output/usr");
        QFile().link("resource/mutable/data", jobDir + "/output/var");

    // Fix some paths
        QFile().link("../fonts", jobDir + "/output/resource/immutable/data/fonts");
        QFile().link("../icons", jobDir + "/output/resource/immutable/data/icons");
        QFile().link("../mime", jobDir + "/output/resource/immutable/data/mime");
        QFile().link("../themes", jobDir + "/output/resource/immutable/data/themes");
        QFile().link(".", jobDir + "/output/resource/mutable/configuration/xdg");


    // Create the instruction script
        QStringList instruction;

        instruction.append("#!/bin/sh -e");
        instruction.append("hash -r");
        instruction.append("export PATH=\"/bin\"");

        instruction.append("export PILE_NAME=\"" + parser.value(name).toUtf8() + "\"");
        instruction.append("export PILE_BRANCH=\"" + jsonDocument.object()["branch"].toString() + "\"");
        instruction.append("export PILE_REVISION=\"" + jsonDocument.object()["revision"].toString() + "\"");
        instruction.append("export PILE_JOB=\"" + id + "\"");
        instruction.append("export PILE_ARCH=\"" + QSysInfo().currentCpuArchitecture() + "\"");

        QString compilerFlags;
        if ( value["toolchain"].toString() == "gnu" || value["toolchain"].toString() == "GNU" ) {
            QMessageLogger().info("Using toolchain : GNU");
            compilerFlags = "-Wall -Ofast -pipe -flto=auto -fstack-clash-protection -fstack-protector-strong -fno-semantic-interposition -fno-plt -fgraphite-identity -floop-nest-optimize -Wl,--sort-common -Wl,--as-needed -Wl,--strip-all -Wl,-z,now -Wl,-z,relro";

            instruction.append("export CC=\"gcc\"");

            instruction.append("export CXX=\"g++\"");

            instruction.append("export LD=\"ld.bfd\"");

            instruction.append("export AR=\"gcc-ar\"");

            instruction.append("export NM=\"gcc-nm\"");

            instruction.append("export RANLIB=\"gcc-ranlib\"");
        }
        else {
            QMessageLogger().info("Using Toolchain : LLVM");
            compilerFlags = "-Wall -Ofast -pipe -flto=thin -fstack-clash-protection -fstack-protector-strong -fno-semantic-interposition -fno-plt -mllvm -polly -Wl,--as-needed -Wl,--strip-all -Wl,-z,now -Wl,-z,relro";

            instruction.append("export CC=\"clang\"");

            instruction.append("export CXX=\"clang++\"");

            instruction.append("export LD=\"ld.lld\"");

            instruction.append("export AR=\"llvm-ar\"");

            instruction.append("export NM=\"llvm-nm\"");

            instruction.append("export RANLIB=\"llvm-ranlib\"");
        }
        if ( QSysInfo().currentCpuArchitecture() == "x86_64" ) {
            compilerFlags += " -m64 -mshstk -fcf-protection";
        }
        QMessageLogger().info("Using flags : %s\n", qUtf8Printable(compilerFlags));

        instruction.append("export CFLAGS=\"" + compilerFlags + "\"");

        instruction.append("export CXXFLAGS=\"" + compilerFlags + "\"");

        instruction.append("export LDFLAGS=\"" + compilerFlags + "\"");

        instruction.append("export MAKEFLAGS=\"-l$(nproc) -j$(($(nproc)+1))\"");

        instruction.append("export INSTALL_ROOT=\"" + jobDir + "/output\"");

        instruction.append("export DESTDIR=\"" + jobDir + "/output\"");

        instruction.append("set -x");
        instruction << value["instruction"].toString();

    // Remove some useless files
        instruction.append("rm -rf \\");
        instruction.append(jobDir + "/output/etc/xdg \\");
        instruction.append(jobDir + "/output/lib/*.la \\");
        instruction.append(jobDir + "/output/lib/charset.alias \\");
        instruction.append(jobDir + "/output/lib/service.d/*/*/supervise \\");
        instruction.append(jobDir + "/output/share/applications \\");
        instruction.append(jobDir + "/output/share/doc \\");
        instruction.append(jobDir + "/output/share/fonts \\");
        instruction.append(jobDir + "/output/share/gettext \\");
        instruction.append(jobDir + "/output/share/gtk-doc \\");
        instruction.append(jobDir + "/output/share/icons \\");
        instruction.append(jobDir + "/output/share/info \\");
        instruction.append(jobDir + "/output/share/mime \\");
        instruction.append(jobDir + "/output/share/polkit-1 \\");
        instruction.append(jobDir + "/output/share/themes \\");
        instruction.append(jobDir + "/output/var/run");

    // Move dbus files from /etc/dbus-1 to /usr/share-dbus-1
        instruction.append("mv " + jobDir + "/output/etc/dbus-1/* /usr/share/dbus-1/");
        instruction.append("rm -r " + jobDir + "/output/etc/dbus-1");

    // Create a symlink for services
        instruction.append("for i in " + jobDir + "/output/lib/service.d/system/* ; do ln -sf /run/runit/supervise.$(basename $i) " + jobDir + "/output/lib/service.d/system/$(basename $i)/supervise; done");
        instruction.append("for i in " + jobDir + "/output/lib/service.d/session/* ; do ln -sf /run/runit/supervise.$(basename $i) " + jobDir + "/output/lib/service.d/session/$(basename $i)/supervise; done");

        instruction.append("cp -rpf " + jobDir + "/output/resource " + outputDir + '/');


        QFile instructionScript(jobDir + "/instruction.sh");
        instructionScript.remove();
        if ( instructionScript.open(QIODevice::ReadWrite | QIODevice::Text) ) {
            QTextStream stream(&instructionScript);
            for ( QString line : instruction ) {
                stream << line << Qt::endl;
            }
            stream << Qt::endl;
            instructionScript.close();
        }
        else {
            QMessageLogger().critical("Could not create the instruction script for %s", qUtf8Printable(id));
            return 1;
        }
        instructionScript.setPermissions(QFileDevice::ReadOwner |
                                         QFileDevice::WriteOwner |
                                         QFileDevice::ExeOwner |
                                         QFileDevice::ReadGroup |
                                         QFileDevice::ExeGroup |
                                         QFileDevice::ReadOther |
                                         QFileDevice::ExeOther);


    // Start the script
        QProcess *instructionProc = new QProcess();
        instructionProc->setProcessChannelMode(QProcess::ForwardedChannels);
        instructionProc->setInputChannelMode(QProcess::ForwardedInputChannel);
        instructionProc->setWorkingDirectory(jobDir);
        instructionProc->setProcessEnvironment(QProcessEnvironment().systemEnvironment());
        instructionProc->start("/bin/sh", QStringList() << "-c" << instructionScript.fileName() );
        instructionProc->waitForFinished(-1);

    // Clean jobDir
        if ( !value["keep-dir"].toBool() ) {
            QDir(jobDir).removeRecursively();
        }

        if ( instructionProc->exitCode() != 0 ) {
            QMessageLogger().critical("\n\"%s\" has failed", qUtf8Printable(id));
            return 1;
        }
        else {
            QMessageLogger().info("\n\"%s\" job has passed successfully", qUtf8Printable(id));
        }
    }


// Clean empileDir
    QDir(empileDir).removeRecursively();


// Copy manifest.json
    QFile().remove(outputDir + "/manifest.json");
    if ( !QFile().copy(manifest, outputDir + "/manifest.json") ) {
        QMessageLogger().critical("Could not copy manifest.json to \"%s\"", qUtf8Printable(outputDir));
        return 1;
    }

    QFile().remove(outputDir + "/manifest.yaml");
    if ( !manifestYaml.isEmpty() && !QFile().copy(manifestYaml, outputDir + "/manifest.yaml") ) {
        QMessageLogger().critical("Could not copy manifest.yaml to \"%s\"", qUtf8Printable(outputDir));
        return 1;
    }

    QMessageLogger().info("The pile directory has been generated at \"%s\"", qUtf8Printable(outputDir));
    return 0;

}
